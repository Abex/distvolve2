#ifndef HOSTCLIENT
#define HOSTCLIENT

#endif // HOSTCLIENT

#include <QTcpSocket>
#include <QString>
#include <distvolve.pb.h>

class Client : QObject
{
public:
  Client(QTcpSocket *sock);
  ~Client();

  void sendPacket(DvProto::Base baseMsg);

  void handleConnect(DvProto::Connect p);
  void handleRequestStatus(DvProto::RequestStatus p);
private:
  QTcpSocket *sock;
  QByteArray data;

  DvProto::Version vers;
  int workers=0;
  QString name;

  bool hasConnected = false;
  bool auth = false;

  int contributions = 0;
};
