#include "modegui.h"
#include "ui_modegui.h"
#include "options.hpp"
#include <QIntValidator>
#include <QValidator>

extern QString winTitle;

ModeGUI::ModeGUI(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ModeGUI)
{
  ui->setupUi(this);

  this->setWindowTitle(winTitle);

  QValidator *portV = new QIntValidator(1,65535,this);

  ui->workerPort->setValidator(portV);
  ui->workerWorkers->setValidator(portV);
  ui->workerHost->setText(Options::options.worker.host);
  ui->workerPort->setText(QString::number(Options::options.worker.port));
  ui->workerWorkers->setText(QString::number(Options::options.worker.workers));
  ui->workerRunCheckBox->setChecked(Options::options.doWorker);

  ui->controllerPort->setValidator(portV);
  ui->controllerPassword->setEchoMode(QLineEdit::Password);
  ui->controllerHost->setText(Options::options.controller.host);
  ui->controllerPort->setText(QString::number(Options::options.controller.port));
  ui->controllerPassword->setText(Options::options.controller.password);
  ui->controllerRunCheckBox->setChecked(Options::options.doController);

  ui->serverPort->setValidator(portV);
  ui->controllerPassword->setEchoMode(QLineEdit::Password);
  ui->serverHost->setText(Options::options.host.hostname);
  ui->serverPort->setText(QString::number(Options::options.host.port));
  ui->controllerPassword->setText(Options::options.host.password);
  ui->serverRunCheckBox->setChecked(Options::options.doHost);
}

void ModeGUI::save()
{
  Options::options.worker.port = ui->workerPort->text().toInt();
  Options::options.worker.host = ui->workerHost->text();
  Options::options.worker.workers = ui->workerWorkers->text().toInt();
  Options::options.doWorker = ui->workerRunCheckBox->checkState()==Qt::Checked;

  Options::options.controller.host = ui->controllerHost->text();
  Options::options.controller.port = ui->controllerPort->text().toInt();
  Options::options.controller.password = ui->controllerPassword->text();
  Options::options.doController = ui->controllerRunCheckBox->checkState()==Qt::Checked;

  Options::options.host.hostname = ui->serverHost->text();
  Options::options.host.password = ui->serverPassword->text();
  Options::options.host.port = ui->serverPort->text().toInt();
  Options::options.host.imageFile = ui->serverImage->text();
  Options::options.doHost = ui->serverRunCheckBox->checkState()==Qt::Checked;
}

ModeGUI::~ModeGUI()
{
  delete ui;
}
