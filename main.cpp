#include <QCoreApplication>
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <thread>
#include <distvolve.pb.h>
#include <options.hpp>
#include <modegui.h>
#include <hostthread.hpp>
#include <controllerwindow.hpp>

DvProto::Version version;

Options::Options Options::options;
QString versionString;
QString winTitle;

void workerThread();

QString versionToString(DvProto::Version version)
{
  return QString("%1.%2.%3").arg(version.major()).arg(version.minor()).arg(version.patch());
}

int main(int argc, char *argv[])
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  version.set_major(2);
  version.set_minor(0);
  version.set_patch(0);

  versionString = versionToString(version);
  winTitle = QString("Distvolve %1").arg(versionString);

  QApplication a(argc,argv);
  a.setApplicationName(winTitle);
  a.setApplicationVersion(versionString);

  QCommandLineParser cliParser;
  cliParser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsCompactedShortOptions);
  cliParser.setApplicationDescription("");
  cliParser.addHelpOption();
  cliParser.addVersionOption();
  cliParser.addOptions(
  {
    {{"no-gui","silent","s"},
      QCoreApplication::translate("main","Do not open the gui")
    },
    {"server",
      QCoreApplication::translate("main","Run the server instance")
    },
    {{"worker","w"},
      QCoreApplication::translate("main","Run the worker instance")
    },
    {{"controller","g"},
      QCoreApplication::translate("main","Run the gui controller")
    },
    {{"host","h"},
      QCoreApplication::translate("main","Host to connect to")
    },
    {{"port","p"},
      QCoreApplication::translate("main","Port to connect to")
    },
    {"server-port",
      QCoreApplication::translate("main","Port to bind to (server)")
    },
    {"server-host",
      QCoreApplication::translate("main","Host to bind to (server)")
    },
    {{"server-image","i"},
      QCoreApplication::translate("main","Image to work on")
    },
    {{"workers","t","threads"},
      QCoreApplication::translate("main","Workers to run")
    },
    {"password",
     QCoreApplication::translate("main","Password for the controller")
    },
    {"server-password",
     QCoreApplication::translate("main","Password for the server")
    }
  });
  cliParser.process(a);

  Options::options.worker.workers = std::thread::hardware_concurrency();
  if(Options::options.worker.workers==0) Options::options.worker.workers = 2;
  Options::options.doWorker = cliParser.isSet("worker");
  Options::options.doController = cliParser.isSet("controller");
  Options::options.doHost = cliParser.isSet("server");
  Options::options.doGui = !cliParser.isSet("no-gui");
  if(cliParser.isSet("h")) Options::options.controller.host = Options::options.worker.host = cliParser.value("host");
  if(cliParser.isSet("port")) Options::options.controller.port = Options::options.worker.port = cliParser.value("port").toInt();
  if(cliParser.isSet("server-host")) Options::options.host.hostname = cliParser.value("server-host");
  if(cliParser.isSet("server-port")) Options::options.host.port = cliParser.value("server-port").toInt();
  if(cliParser.isSet("workers")) Options::options.worker.workers = cliParser.value("workers").toInt();
  if(cliParser.isSet("server-image")) Options::options.host.imageFile = cliParser.value("server-image");
  if(cliParser.isSet("server-password")) Options::options.host.password = cliParser.value("server-password");
  if(cliParser.isSet("password")) Options::options.controller.password = cliParser.value("password");

  bool hasMode = (Options::options.doHost||Options::options.doController||Options::options.doWorker);

  bool showGUI = false;
  if(!Options::options.doGui)
    showGUI=false;
  else if(!hasMode)
    showGUI=true;

  if(showGUI)
  {
    ModeGUI modeGui(0);
    modeGui.show();
    modeGui.exec();
    if(modeGui.result()==QDialog::Rejected) return 0;
    modeGui.save();
  }
  if(!(Options::options.doHost||Options::options.doController||Options::options.doWorker))
  {
    qCritical("No mode set");
    return 0;
  }

  qDebug() << "Starting " << winTitle;
  qDebug() << QString("Starting %1 %2 %3").arg((Options::options.doWorker)?"Worker":"").arg((Options::options.doHost)?"Server":"").arg((Options::options.doController)?"Controller":"");

  HostThread *hs = NULL;
  if(Options::options.doHost) {hs = new HostThread; hs->start();}
  if(Options::options.doController)
  {// connect
    ControllerWindow cw;
    connect(&cw,&ControllerWindow::sigError,[](){
      exit(1);
    });
    cw.exec();
  }
  //if(Options::options.doWorker) std::thread(workerThread).detach();

  if(hs && hs->isFinished())
  {
    hs->deleteLater();
    hs=NULL;
  }

  return a.exec();
}
