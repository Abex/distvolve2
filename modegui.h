#ifndef MODEGUI_H
#define MODEGUI_H

#include <qdialog.h>

namespace Ui {
  class ModeGUI;
}

class ModeGUI : public QDialog
{
  Q_OBJECT

public:
  explicit ModeGUI(QWidget *parent = 0);
  void save();
  ~ModeGUI();

private:
  Ui::ModeGUI *ui;
};

#endif // MODEGUI_H
