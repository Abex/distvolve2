#ifndef CONTROLLERWINDOW_HPP
#define CONTROLLERWINDOW_HPP

#include <QDialog>
#include <QObject>
#include <QTcpSocket>
#include <distvolve.pb.h>

namespace Ui {
  class ControllerWindow;
}

class ControllerWindow : public QDialog
{
  Q_OBJECT
public:
  explicit ControllerWindow(QWidget *parent = 0);
  bool failed();
  ~ControllerWindow();

public slots:
  void onData();
  void onError(QAbstractSocket::SocketError error);

//public signals:
  void sigError();

private:
  void handleConnectAck(DvProto::ConnectAck);
  void handleConnectNack(DvProto::ConnectNack);
  void handleStatus(DvProto::Status);
  void setupGui();
  void sendPacket(DvProto::Base);

  bool fail=false;
  bool hasConnected=false;

  Ui::ControllerWindow *ui;
  QTcpSocket *sock;
  QByteArray data;
};

#endif // CONTROLLERWINDOW_HPP
