#include <controllerwindow.hpp>
#include <ui_controllerwindow.h>
#include <QString>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QObject>
#include <options.hpp>
#include <QMessageBox>

extern DvProto::Version version;
extern QString winTitle;

void MsgError(QString error)
{
  QMessageBox m;
  m.setWindowTitle(winTitle);
  m.setIcon(QMessageBox::Critical);
  m.setText(error);
  m.exec();
}

ControllerWindow::ControllerWindow(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ControllerWindow)
{
  sock = new QTcpSocket(this);
  connect(sock,&QTcpSocket::readyRead,this,&onData);
  connect(sock,&QTcpSocket::disconnected,[=](){
    MsgError("Controller Disconnected");
    this->deleteLater();
  });
  connect(sock,&QTcpSocket::connected,[=](){
    DvProto::Base baseMsg;
    baseMsg.set_type(DvProto::Base_Type_Connect);
    DvProto::Connect *conMsg = baseMsg.MutableExtension(DvProto::Connect::msg);
    conMsg->set_name("Controller Client");
    conMsg->set_workers(0);
    conMsg->set_password(Options::options.controller.password.toStdString());
    conMsg->set_allocated_version(&version);
    sendPacket(baseMsg);
  });
  QObject::connect(sock,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(onError(QAbstractSocket::SocketError)));
  sock->connectToHost(Options::options.controller.host,Options::options.controller.port);
}

void ControllerWindow::onError(QAbstractSocket::SocketError error)
{
  if(error==QAbstractSocket::HostNotFoundError)
    MsgError("Controller: Host not found");
  else if(error==QAbstractSocket::ConnectionRefusedError)
    MsgError("Controller: Connection Refused");
  else
    MsgError(QString("Controller: Socket Error %1").arg((int)error));
  fail = true;
  this->deleteLater();
}

void ControllerWindow::setupGui()
{
  ui->setupUi(this);
  this->setWindowFlags(Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint | Qt::WindowMinimizeButtonHint);

  this->setWindowTitle(winTitle);
}

void ControllerWindow::sendPacket(DvProto::Base baseMsg)
{ // same as hostclient.cpp / Client::sendPacket(DvProto::Base);
  std::string output;
  baseMsg.SerializeToString(&output);
  quint32 len = output.length();
  QByteArray withHeader(len+4,'\0');
  withHeader.setNum(len,0);
  withHeader.append(output.data(),len);
  sock->write(withHeader);
}

void ControllerWindow::onData()
{
  data += sock->readAll();
  if(data.length()<4) return;
  DvProto::Base packet;
  quint32 len = *((quint32*)data.constData());
  if(data.length()<len+4) return;
  packet.ParseFromArray(data.constData()+4,len);
  data.remove(0,len+4);
  if(!packet.has_type()) return;
  switch(packet.type())
  {
  case DvProto::Base_Type_ConnectAck:
    handleConnectAck(packet.GetExtension(DvProto::ConnectAck::msg));
    break;
  case DvProto::Base_Type_ConnectNack:
    handleConnectNack(packet.GetExtension(DvProto::ConnectNack::msg));
    break;
  case DvProto::Base_Type_Status:
    handleStatus(packet.GetExtension(DvProto::Status::msg));
    break;
  default:
    qWarning() << QString("Bad Packet: %1").arg(packet.type());
  }
}

void ControllerWindow::handleConnectAck(DvProto::ConnectAck ack)
{
  if(hasConnected) return;
  hasConnected=true;

  setupGui();
}

void ControllerWindow::handleConnectNack(DvProto::ConnectNack error)
{
  QString r;
  if(!error.has_reason())
    r="Unknown reason";
  else
    r=QString::fromStdString(error.reason());
  MsgError(QString("Controller: Connection refused - %1").arg(r));
  this->deleteLater();
}

void ControllerWindow::handleStatus(DvProto::Status)
{
  //update gui
}

bool ControllerWindow::failed()
{
  return fail;
}

ControllerWindow::~ControllerWindow()
{
  delete ui;
}
