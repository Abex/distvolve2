#include <hostclient.h>
#include <distvolve.pb.h>
#include <options.hpp>
#include <string>
#include <QList>

extern QList<Client*> clients;
extern DvProto::Version version;

void Client::sendPacket(DvProto::Base baseMsg)
{
  std::string output;
  baseMsg.SerializeToString(&output);
  quint32 len = output.length();
  QByteArray withHeader(len+4,'\0');
  withHeader.setNum(len,0);
  withHeader.append(output.data(),len);
  sock->write(withHeader);
}

void Client::handleConnect(DvProto::Connect p)
{
  if(p.has_version() && p.version().major()==version.major() && p.version().minor()==version.minor())
  {
    vers = p.version();
  }
  else
  {
    DvProto::Base baseMsg;
    baseMsg.set_type(DvProto::Base_Type_ConnectNack);
    DvProto::ConnectNack* nackMsg = baseMsg.MutableExtension(DvProto::ConnectNack::msg);
    nackMsg->set_reason("Bad Version");
    sendPacket(baseMsg);
    sock->disconnect();
    this->deleteLater();
    return ;
  }
  if(!Options::options.host.password.isEmpty())
  {
    if(p.has_password() && p.password()!=Options::options.host.password.toStdString())
    {
      DvProto::Base baseMsg;
      baseMsg.set_type(DvProto::Base_Type_ConnectNack);
      DvProto::ConnectNack* nackMsg = baseMsg.MutableExtension(DvProto::ConnectNack::msg);
      nackMsg->set_reason("Bad Password");
      sendPacket(baseMsg);
      sock->disconnect();
      this->deleteLater();
      return ;
    }
    else
      auth=true;
    //no password send > no auth (but can connect)
  }
  else
   auth=true;

  if(p.has_name())
    name = QString::fromStdString(p.name());
  else
    name = QString("Client %1").arg(clients.indexOf(this));

  if(p.has_workers()) workers = p.workers();

  hasConnected=true;

  DvProto::Base baseMsg;
  baseMsg.set_type(DvProto::Base_Type_ConnectAck);
  DvProto::ConnectAck *ackMsg = baseMsg.MutableExtension(DvProto::ConnectAck::msg);
  ackMsg->set_name(name.toStdString());
  ackMsg->set_allocated_version(&version);
  sendPacket(baseMsg);
}

void Client::handleRequestStatus(DvProto::RequestStatus p)
{
  DvProto::Base baseMsg;
  baseMsg.set_type(DvProto::Base_Type_Status);
  DvProto::Status *stats = baseMsg.MutableExtension(DvProto::Status::msg);
  foreach(const Client* c,clients)
  {
    DvProto::StatusClient *sc = stats->add_clients();
    sc->set_name(c->name.toStdString());
    sc->set_workers(c->workers);
    sc->set_allocated_version(&vers);
    sc->set_contributions(c->contributions);
  }
  sendPacket(baseMsg);
}
