#-------------------------------------------------
#
# Project created by QtCreator 2015-01-22T20:07:01
#
#-------------------------------------------------

QT       += core gui widgets network

TARGET = Distvolve
CONFIG   += qt console

LIBS += -Ld:/protobuf260/lib -lprotobuf

INCLUDEPATH += $$PWD d:/protobuf260/include

QMAKE_CXXFLAGS += -Wno-unused-parameter -Wno-sign-compare -std=gnu++11

TEMPLATE = app


SOURCES += main.cpp \
    modegui.cpp \
    hostthread.cpp \
    distvolve.pb.cc \
    hostclient.cpp \
    controllerwindow.cpp

OTHER_FILES += \
    distvolve.proto

HEADERS += \
    options.hpp \
    modegui.h \
    hostthread.hpp \
    distvolve.pb.h \
    hostclient.h \
    controllerwindow.hpp

FORMS += \
    modegui.ui \
    controllerwindow.ui
