#include <options.hpp>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostInfo>
#include <hostthread.hpp>
#include <distvolve.pb.h>
#include <string>
#include <QList>
#include <hostclient.h>

class Client;

QList<Client*> clients;

Client::Client(QTcpSocket *sock_)
{
  qDebug() << "Client connecting";
  sock=sock_;
  connect(sock,&QTcpSocket::readyRead,[this](){
    data += sock->readAll();
    if(data.length()<4) return;
    DvProto::Base packet;
    quint32 len = *((quint32*)data.constData());
    if(data.length()<len+4) return;
    packet.ParseFromArray(data.constData()+4,len);
    data.remove(0,len+4);
    if(!packet.has_type()) return;
    switch(packet.type())
    {
    case DvProto::Base_Type_Connect:
      handleConnect(packet.GetExtension(DvProto::Connect::msg));
      break;
    case DvProto::Base_Type_RequestStatus:
      handleRequestStatus(packet.GetExtension(DvProto::RequestStatus::msg));
      break;
    default:
      qWarning() << QString("Bad Packet: %1").arg(packet.type());
    }
  });
}
Client::~Client()
{
  sock->deleteLater();
}

HostThread::HostThread()
{}

void HostThread::run()
{
  QHostAddress* addr;
  if(Options::options.host.hostname.isEmpty())
    addr = new QHostAddress(QHostAddress::Any);
  else
    addr = &QHostInfo::fromName(Options::options.host.hostname).addresses()[0];

  QTcpServer *server = new QTcpServer;
  connect(server,&QTcpServer::newConnection,[=](){
    QTcpSocket *sock = server->nextPendingConnection();
    clients.append(new Client(sock));
  });
  server->listen(*addr,Options::options.host.port);
  exec();
}
