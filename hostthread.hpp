#ifndef HOSTTHREAD_HPP
#define HOSTTHREAD_HPP

#include <QThread>

class HostThread : public QThread
{
public:
  HostThread();
  void run();
};

#endif // HOSTTHREAD_HPP

