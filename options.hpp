#ifndef OPTIONS_HPP
#define OPTIONS_HPP

#include <QString>

namespace Options {
  struct HostInfo {
    QString hostname="";
    int port=2529;
    QString imageFile="";
    QString password="";
  };
  struct WorkerInfo {
    QString host="localhost";
    int port=2529;
    int workers;
  };
  struct ControllerInfo {
    QString host="localhost";
    QString password="";
    int port=2529;
  };

  struct Options {
    bool doHost=false;
    HostInfo host;
    bool doWorker=false;
    WorkerInfo worker;
    bool doController=false;
    ControllerInfo controller;

    bool doGui=true;
  };
  extern Options options;
}

#endif // OPTIONS_HPP

